#ifndef ICONFIGUTATIONDAO
#define ICONFIGUTATIONDAO

#include "iconfiguration.h"

class IConfigurationDAO
{
    virtual IConfiguration* GetConfiguration() = 0;
    virtual void UpdateConfiguration( IConfiguration* configuration ) = 0;
};

#endif // ICONFIGUTATION

