#include "printhead.h"

Printhead::Printhead(IConfiguration *configuration)
{
    widthNozzle = configuration->GetWidthNozzle();
    layerThickness = configuration->GetLayerThicness();
    minHeight = configuration->GetMinHeightPositioning();
    maxHeight = configuration->GetMaxHeightPositioning();
}



int Printhead::getWidthNozzle()
{
    return widthNozzle;
}

int Printhead::getLayerThickness()
{
    return layerThickness;
}

int Printhead::getMinHeight()
{
    return minHeight;
}

int Printhead::getMaxHeight()
{
    return maxHeight;
}

