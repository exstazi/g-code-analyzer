#include "xmldaofactory.h"

XMLConfigurationDAO* XMLDAOFactory::_config = NULL;

IConfigurationDAO* XMLDAOFactory::GetConfigurationDAO()
{
    if (!_config)
    {
        _config = new XMLConfigurationDAO();
    }
    return _config;
}
