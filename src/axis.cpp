#include "axis.h"

Axis::Axis(IConfiguration* configuration)
{
    _maxSpeed = configuration->GetMaxSpeedAxis();
    _speedG00 = configuration->GetSpeedG00Axis();
    _acceleration = configuration->GetAcceleration();
    _upperLimitPositioning = configuration->GetUpperLimitPositioning();

    _currentPosition = _currentSpeed = 0;
}

void Axis::move(double position )
{
    if (position<0)
        return;
    _currentPosition = position;
}

void Axis::move(double position, double currentSpeed)
{
    _currentPosition += position;
    _currentSpeed = currentSpeed;
}

double Axis::getCurrentPosition()
{
    return _currentPosition;
}

double Axis::getCurrentSpeed()
{
    return _currentSpeed;
}

long Axis::getMaxSpeed()
{
    return _maxSpeed;
}

long Axis::getSpeedG00()
{
    return _speedG00;
}

long Axis::getAcceleration()
{
    return _acceleration;
}

long Axis::getUpperLimitPositioning()
{
    return _upperLimitPositioning;
}

