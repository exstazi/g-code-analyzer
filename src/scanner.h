#ifndef SCANNER_H
#define SCANNER_H

#include "iconfiguration.h"

class Scanner
{
protected:
    long maxSpeed;
    int widthLine;
    int minHeight;
    int maxHeight;

public:
    Scanner( IConfiguration *configuration );
    long getMaxSpeed();
    int getWidthLine();
    int getMinHeight();
    int getMaxHeight();
};

#endif // SCANNER_H
