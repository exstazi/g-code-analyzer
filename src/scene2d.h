#ifndef SCENE2D_H
#define SCENE2D_H

#include <QWidget>
#include <QPainter>
#include <QList>
#include <QVector2D>

class Scene2D : public QWidget
{
    Q_OBJECT
public:
    explicit Scene2D(QWidget *parent = 0);
    void setCurrentLayer(int layer);
public slots:
    void changedPosition(double x, double y );
    void newLayer(bool isPrint);
protected slots:
    void paintEvent(QPaintEvent *);

protected:
    bool _isPrint;
    int _countLayer;
    int _currentLayer;
    QList<QList<QVector2D>* > *_layers;
};

#endif // SCENE2D_H
