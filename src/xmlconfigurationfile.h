#ifndef XMLCONFIGURATIONFILE_H
#define XMLCONFIGURATIONFILE_H

#include <QFile>
#include <QMap>
#include <QXmlStreamReader>
#include <QCryptographicHash>

#include "iconfiguration.h"

class XMLConfigurationFile: public IConfiguration
{
public:
    XMLConfigurationFile( QFile *file );

    QString GetAdminPwd();
    int GetWidthNozzle();
    int GetLayerThicness();
    int GetMinHeightPositioning();
    int GetMaxHeightPositioning();
    int GetMaxSpeedScanner();
    int GetMaxSpeedPrinthead();
    int GetWidthScanner();
    int GetMaxSpeedAxis();
    int GetSpeedG00Axis();
    int GetAcceleration();
    int GetUpperLimitPositioning();
    int GetLowerLimitPositioning();

    void SetWidthNozzle( int value );
    void SetLayerThicness( int value );
    void SetMinHeightPositioning( int value );
    void SetMaxHeightPositioning( int value );
    void SetMaxSpeedScanner( int value );
    void SetMaxSpeedPrinthead( int value );
    void SetWidthScanner( int value );
    void SetMaxSpeedAxis( int value );
    void SetSpeedG00Axis( int value );
    void SetAcceleration( int value );
    void SetUpperLimitPositioning( int value );
    void SetLowerLimitPositioning( int value );
    void SetAdminPwd( QString oldPass, QString newPass );

    void save(QString nameFile);

protected:
    void loadFile();

protected:
    QFile* _file;
    QString _adminPwd;
    QString _salt;
    QMap<QString, int> xmlMap;
};

#endif // XMLCONFIGURATIONFILE_H
