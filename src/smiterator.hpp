#ifndef SMITERATOR
#define SMITERATOR

#include <QStack>
#include <QDebug>
#include "statemachine.hpp"

template <typename Transition>
class StateMachine;

/**
 *  Итератор для контейнера StateMachine.
 *  Выполняет обход в глубину
 *  @template Transition тип токенов
 *  @param _transition
 *  @param P - указатель на структуру состояния
 *  @param _stack - стек для хранения последовательности переходов
 */
template <typename Transition>
class SMIterator
{
    friend class StateMachine<Transition>;
public:
    SMIterator( NodeState<Transition> *p )
    {
        this->p = p;
    }

    SMIterator& operator =(const SMIterator &other );
    SMIterator& operator ++();

    NodeState<Transition>* operator *()
    {
        return p;
    }

    Transition* operator ->()
    {
        return (_transition->_rule);
    }

    bool operator ==(const SMIterator &other )
    {
        return ( (p == other.p) && (_stack.size() == other._stack.size()) );
    }

    bool operator !=(const SMIterator &other)
    {
        return !(*this == other);
    }

private:
    NodeTransition<Transition> *_transition;
    NodeState<Transition> *p;
    QStack<NodeState<Transition>*> _stack;
};

/**
 *  Переход к следующему элементу по алгоритму обхода в глубину.
 *  @return итератор для следуюшего элемента
 */
template <typename Transition>
SMIterator<Transition>& SMIterator<Transition>::operator ++()
{
    NodeTransition<Transition> *next = p->_transitions;
    if (!next->_state->_transitions) {

        p = _stack.pop();
        return *this;
    }
    while (next)
    {
        _stack.push(next->_state);
        next = next->_nextTransition;
    }
    p = _stack.pop();
    while (!p->_transitions )
    {
        p = _stack.pop();
    }
    return *this;
}

/**
 *  Сравнение итератора
 *  @param other - сравниваемый итератор
 */
template <typename Transition>
SMIterator<Transition>& SMIterator<Transition>::operator =(const SMIterator<Transition> &other )
{
    if ( this != &other )
    {
        p = other.p;
    }
    return *this;
}

#endif // SMITERATOR

