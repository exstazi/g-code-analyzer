#ifndef XMLCONFIGURATIONDAO_H
#define XMLCONFIGURATIONDAO_H

#include "iconfigutationdao.h"
#include "xmlconfigurationfile.h"

class XMLConfigurationDAO: public IConfigurationDAO
{
public:
    XMLConfigurationDAO();
    IConfiguration *GetConfiguration();
    void UpdateConfiguration(IConfiguration* configuration );

protected:
    static IConfiguration *configurationXmlFile;
};

#endif // XMLCONFIGURATIONDAO_H
