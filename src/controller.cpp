#include "controller.h"
#include <QDebug>
#include "exception.h"
#include <QMetaType>

Controller::Controller(Workstation *workstation)
{
    _commands = new QList<Command*>();
    _stateMachine = new StateMachine<Command*>();

    try
    {
        _stateMachine->deserialize( "states.json");
        _stateMachine->setInitState( "Init" );
        _stateMachine->setEndState( "End" );
    }
    catch( IllegalStateException ex )
    {
        qDebug() << ex.toString();
    }



    _workstation = workstation;
    _currentCommand = 0;
    _countCycleScannig = _countMfunction = _countLayer = _time = 0;
}

void Controller::execute()
{
    foreach (Command *command, *_commands) {
        command->execute();
        emit executedCommand(command);
    }
}

void Controller::executeCommand()
{
    if (_currentCommand > _commands->size()-1 )
    {
        return ;
    }
    _commands->at( _currentCommand )->execute();
    emit executedCommand(_commands->at(_currentCommand));
    ++_currentCommand;

}

QList<Command*>* Controller::getCommands()
{
    return _commands;
}

void Controller::checkCommand()
{
    _stateMachine->reset();
    _currentCheck = 0;
    IComparate<Command*> *compare = new ICompareCommand();
    foreach ( Command *command, *_commands ) {
        _stateMachine->checkToken( command, compare );
        ++_currentCheck;


    }
    if (!_stateMachine->isEndState() )
    {
        throw IllegalStateException( "State machine is not end state.");
    }
}

void Controller::reset()
{
    _currentCheck = 0;
}

int Controller::getTime()
{
    return _time;
}

int Controller::getNumberCommand()
{
    return _currentCommand;
}

int Controller::getCountLayer()
{
    return _countLayer;
}

int Controller::getCountCycleScannig()
{
    return _countCycleScannig;
}

int Controller::getCountMfunction()
{
    return _countMfunction;
}

void Controller::processFile(QString nameFile)
{
    QFile *file = new QFile( nameFile );
    if ( !file->open( QIODevice::ReadOnly ) )
    {
        throw FileNotFoundException( "Controller::processFile, gcode file not found.");
    }
    _commands->clear();
    Command *command = NULL;
    QByteArray line;
    while( !file->atEnd() )
    {
        line = file->readLine().trimmed();
        command = parseCommand( getParametrsCommand( line ) );
        if (command)
        {
            command->setLine( line );
            _commands->append( command );
        }
    }
}

void Controller::setCommand(QList<QString> commands)
{
    _countCycleScannig = _countMfunction = _countLayer = _time = 0;
    _commands->clear();
    Command *command = NULL;
    foreach (QString line, commands) {
        command = parseCommand( getParametrsCommand( line.toUtf8() ) );
        if( command )
        {
            command->setLine( line );
            command->setWorkstation( _workstation );
            if (command->getName() == "M20" )
            {
                ++_countLayer;
            }
            if (command->getName() == "M22" )
            {
                ++_countCycleScannig;
            }
            _commands->append( command );
        }
    }
}

Command* Controller::parseCommand(QMap<char, double> *parameters)
{
    if( parameters->contains( 'G' ) )
    {
        ++_time;
        return Command::getInstance( "G" + QString::number( parameters->value('G') ),
                                     QVector3D( parameters->value('X',-1.0),
                                                parameters->value('Y',-1.0),
                                                parameters->value('Z',-1.0) ) );
    }
    else if ( parameters->contains( 'M' ) )
    {
        ++_countMfunction;
        return Command::getInstance( "M" + QString::number( parameters->value('M') ) );
    }
    return NULL;
}



QMap<char, double>* Controller::getParametrsCommand(QByteArray lineCommand)
{
    QMap<char, double> *map = new QMap<char, double>();
    foreach (QByteArray element, lineCommand.split( ' ' ) )
    {
        if( element.size() > 1 )
        {
            map->insert( element.at(0), element.right( element.size() - 1 ).toDouble() );
        }
    }
    return map;
}

