#include "xmlconfigurationfile.h"

#include "exception.h"

#include <QDebug>

XMLConfigurationFile::XMLConfigurationFile(QFile *file)
{
    _file = file;

    if (!file->open(QIODevice::ReadOnly | QIODevice::Text))
    {
        throw FileNotFoundException( "Невозможно открыть XML конфиг" );
    }
    xmlMap.insert( "widthNozzle", -1 );
    xmlMap.insert( "layerThickness", -1 );
    xmlMap.insert( "maxSpeedPrintHead", -1 );
    xmlMap.insert( "minHeightPositioning", -1 );
    xmlMap.insert( "maxHeightPositioning", -1 );
    xmlMap.insert( "maxSpeedScanner", -1 );
    xmlMap.insert( "widthScanner", -1 );
    xmlMap.insert( "maxSpeedAxis", -1 );
    xmlMap.insert( "speedG00Axis", -1 );
    xmlMap.insert( "acceleration", -1 );
    xmlMap.insert( "upperLimitPositioning", -1 );
    xmlMap.insert( "lowerLimitPositioning", -1 );
    loadFile();
}


void XMLConfigurationFile::loadFile()
{
    QXmlStreamReader *_reader = new QXmlStreamReader( _file );
    while (!_reader->atEnd() && !_reader->hasError())
    {
       QXmlStreamReader::TokenType token = _reader->readNext();
       if (token == QXmlStreamReader::StartDocument)
           continue;
       if (token == QXmlStreamReader::StartElement)
       {
           if (_reader->name() == "config")
           {
               continue;
           }
           if (_reader->name() == "adminPwd")
           {
               _reader->readNext();
               _adminPwd = _reader->text().toString();
               continue ;
           }
           if (_reader->name() == "salt")
           {
               _reader->readNext();
               _salt = _reader->text().toString();
               continue ;
           }

           QString name = _reader->name().toString();
           _reader->readNext();
           xmlMap.insert( name, _reader->text().toInt() );
       }
    }
    _file->close();
    delete _file;

    foreach (int value, xmlMap.values()) {
        if( value == -1 )
        {
            throw MalformedXMLException();
        }
    }
}

void XMLConfigurationFile::save( QString nameFile )
{
    _file = new QFile( nameFile );

    if( !_file->open( QIODevice::WriteOnly | QIODevice::Truncate  ) )
    {
        qDebug() << "File not Open";
        qDebug() << _file->errorString();
    }

    QXmlStreamWriter *xmlWriter = new QXmlStreamWriter( _file );
    xmlWriter->writeStartDocument();
    xmlWriter->writeStartElement( "config" );
    xmlWriter->writeStartElement( "adminPwd" );
    xmlWriter->writeCharacters( _adminPwd );
    xmlWriter->writeEndElement();
    xmlWriter->writeStartElement( "salt" );
    xmlWriter->writeCharacters( _salt );
    xmlWriter->writeEndElement();
    foreach (QString name, xmlMap.keys() ) {
        xmlWriter->writeStartElement( name );
        xmlWriter->writeCharacters( QString::number( xmlMap.value( name, 0 ) ) );
        xmlWriter->writeEndElement();
    }
    xmlWriter->writeEndElement();
    xmlWriter->writeEndDocument();
    _file->flush();
    _file->close();

}

QString XMLConfigurationFile::GetAdminPwd()
{
    return _adminPwd;
}

void XMLConfigurationFile::SetAdminPwd(QString oldPass, QString newPass)
{
    if (_adminPwd == QCryptographicHash::hash( oldPass.toUtf8(), QCryptographicHash::Sha3_512 ).toHex() )
    {
        _adminPwd = QCryptographicHash::hash( newPass.toUtf8(), QCryptographicHash::Sha3_512 ).toHex();
    }
}

int XMLConfigurationFile::GetWidthNozzle()
{
    return xmlMap.find( "widthNozzle" ).value();
}

void XMLConfigurationFile::SetWidthNozzle(int value)
{
    xmlMap.insert( "widthNozzle", value );
}

int XMLConfigurationFile::GetLayerThicness()
{
    return xmlMap.find( "layerThickness" ).value();
}

void XMLConfigurationFile::SetLayerThicness(int value)
{
    xmlMap.insert( "layerThickness", value );
}

int XMLConfigurationFile::GetMinHeightPositioning()
{
    return xmlMap.find( "minHeightPositioning" ).value();
}

void XMLConfigurationFile::SetMinHeightPositioning(int value)
{
    xmlMap.insert( "minHeightPositioning", value );
}

int XMLConfigurationFile::GetMaxHeightPositioning()
{
    return xmlMap.find( "maxHeightPositioning" ).value();
}

void XMLConfigurationFile::SetMaxHeightPositioning(int value)
{
    xmlMap.insert( "maxHeightPositioning", value );
}

int XMLConfigurationFile::GetMaxSpeedScanner()
{
    return xmlMap.find( "maxSpeedScanner" ).value();
}

void XMLConfigurationFile::SetMaxSpeedScanner(int value)
{
    xmlMap.insert( "maxSpeedScanner", value );
}

int XMLConfigurationFile::GetMaxSpeedPrinthead()
{
    return xmlMap.find( "maxSpeedPrintHead" ).value();
}

void XMLConfigurationFile::SetMaxSpeedPrinthead(int value)
{
    xmlMap.insert( "maxSpeedPrintHead", value );
}

int XMLConfigurationFile::GetWidthScanner()
{
    return xmlMap.find( "widthScanner" ).value();
}

void XMLConfigurationFile::SetWidthScanner(int value)
{
    xmlMap.insert( "widthScanner", value );
}

int XMLConfigurationFile::GetMaxSpeedAxis()
{
    return xmlMap.find( "maxSpeedAxis" ).value();
}

void XMLConfigurationFile::SetMaxSpeedAxis(int value)
{
    xmlMap.insert( "maxSpeedAxis", value );
}

int XMLConfigurationFile::GetSpeedG00Axis()
{
    return xmlMap.find( "speedG00Axis" ).value();
}

void XMLConfigurationFile::SetSpeedG00Axis(int value)
{
    xmlMap.insert( "speedG00Axis", value );
}

int XMLConfigurationFile::GetAcceleration()
{
    return xmlMap.find( "acceleration" ).value();
}

void XMLConfigurationFile::SetAcceleration(int value)
{
    xmlMap.insert( "acceleration", value );
}

int XMLConfigurationFile::GetUpperLimitPositioning()
{
    return xmlMap.find( "upperLimitPositioning" ).value();
}

void XMLConfigurationFile::SetUpperLimitPositioning(int value)
{
    xmlMap.insert( "upperLimitPositioning", value );
}

int XMLConfigurationFile::GetLowerLimitPositioning()
{
    return xmlMap.find( "lowerLimitPositioning" ).value();
}

void XMLConfigurationFile::SetLowerLimitPositioning(int value)
{
    xmlMap.insert( "lowerLimitPositioning", value );
}

