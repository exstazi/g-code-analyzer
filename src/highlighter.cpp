#include "highlighter.h"
#include <iostream>


Highlighter::Highlighter(QTextDocument *parent)
    : QSyntaxHighlighter(parent)
{

    anyText.setForeground( QColor( 0,170,0 ) );


    keywordFormat.setForeground( QColor( 183,183,0 ) );
    keywordFormat.setFontWeight(QFont::Bold);
    QStringList keywordPatterns;
    keywordPatterns << "\\bG-1\\b" << "\\bG-2\\b";

    //
    QTextCharFormat mFunc,
                    comment,
                    axis;
    mFunc.setForeground( QColor( "#a11a11" ) );
    comment.setForeground( QColor( "#555555" ) );
    axis.setFontItalic(true);
    axis.setForeground( QColor(108,108,183) );

    highlightingRules.append( HighlightingRule( QRegExp("G[^\n\r\t ]*"), anyText ) );
    highlightingRules.append( HighlightingRule( QRegExp("M[^\n\r\t ]*"), mFunc ) );
    highlightingRules.append( HighlightingRule( QRegExp("X[^\n\r\t ]*"), axis ) );
    highlightingRules.append( HighlightingRule( QRegExp("Y[^\n\r\t ]*"), axis ) );
    highlightingRules.append( HighlightingRule( QRegExp("Z[^\n\r\t ]*"), axis ) );
    highlightingRules.append( HighlightingRule( QRegExp("E[^\n\r\t ]*"), axis ) );

    highlightingRules.append( HighlightingRule( QRegExp(";[^\n]*"),comment ) );
    highlightingRules.append( HighlightingRule( QRegExp("\'.*\'"),comment ) );



    foreach (const QString &pattern, keywordPatterns) {
        highlightingRules.append( HighlightingRule( QRegExp( pattern ),keywordFormat ) );
    }

    commentStartExpression = QRegExp("/\\*");
    commentEndExpression = QRegExp("\\*/");

}


void Highlighter::highlightBlock(const QString &text)
{
    foreach (const HighlightingRule &rule, highlightingRules) {
        QRegExp expression(rule.pattern);
        int index = expression.indexIn(text);
        while (index >= 0) {
            int length = expression.matchedLength();
            setFormat(index, length, rule.format);
            index = expression.indexIn(text, index + length);
        }
    }

    setCurrentBlockState(0);

    int startIndex = 0;
    if (previousBlockState() != 1)
        startIndex = commentStartExpression.indexIn(text);


    while (startIndex >= 0) {

        int endIndex = commentEndExpression.indexIn(text, startIndex);
        int commentLength;
        if (endIndex == -1) {
            setCurrentBlockState(1);
            commentLength = text.length() - startIndex;
        } else {
            commentLength = endIndex - startIndex
                            + commentEndExpression.matchedLength();
        }
        setFormat(startIndex, commentLength, anyText);
        startIndex = commentStartExpression.indexIn(text, startIndex + commentLength);
    }



}

