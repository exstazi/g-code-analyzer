#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <QString>

class Exception
{
public:
    Exception( QString description = QString() )
    {
        _description = description;
    }

    QString toString()
    {
        return _description;
    }

protected:
    QString _description;
};

class FileNotFoundException: public Exception
{
public:
    FileNotFoundException( QString description = QString( "File not found") )
        : Exception( description ){}
};

class MalformedXMLException: public Exception
{
public:
    MalformedXMLException( QString description = QString( "Malformed xml" ) )
        :Exception( description ){}
};

class IllegalStateException: public Exception
{
public:
    IllegalStateException( QString description = QString( "Illegal state" ) )
        : Exception( description ){}
};

class JSONStateException: public Exception
{
public:
    JSONStateException( QString description = QString( "Json state exception") )
        : Exception( description ){}
};

#endif // EXCEPTION

