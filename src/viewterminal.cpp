#include "viewterminal.h"
#include "ui_viewterminal.h"

#include <QDebug>
#include <QFileDialog>
#include <QFile>
#include <QCryptographicHash>
#include "xmldaofactory.h"
#include "codeeditor.h"
#include "highlighter.h"
#include "workstation.h"
#include "exception.h"

#include <QPushButton>

ViewTerminal::ViewTerminal(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ViewTerminal)
{
    ui->setupUi(this);
    ui->txtTypeAccount->setVisible( false );
    ui->labelEnter->setVisible( false );

    try
    {
        configWorkstation = (XMLConfigurationFile*)((XMLConfigurationDAO*) XMLDAOFactory::GetConfigurationDAO())->GetConfiguration();
    }
    catch (FileNotFoundException )
    {
        ui->txtErrorMessage->setText( "Configuration file not found." );
        ui->enter_operator->setEnabled( false );
        ui->enter_config->setEnabled( false );

        return ;
    }
    catch (MalformedXMLException )
    {
        ui->txtErrorMessage->setText( "Malformed configuration file." );
        ui->enter_operator->setEnabled( false );
        ui->enter_config->setEnabled( false );

        return ;
    }

    workstation = new Workstation( configWorkstation );
    printInfoOfPrinter();

    try
    {
        controller = new Controller( workstation );
    }
    catch(JSONStateException ex )
    {
        ui->txtErrorMessage->setText( ex.toString() );
        ui->enter_operator->setEnabled( false );
        ui->enter_config->setEnabled( false );
        return ;
    }


    codeEditor = new CodeEditor(this);
    new Highlighter( codeEditor->document() );
    scene3D = new Scene3D(this);
    _timer = new QTimer();

    initUI();
}


void ViewTerminal::resizeEvent(QResizeEvent *event)
{
    ui->horizontalLayoutWidget->setGeometry( QRect( 30,0, event->size().width() - 60, 30 ) );
    ui->about->setGeometry( event->size().width() / 2 - 150, event->size().height() / 2 - 130, 300, 150 );
    ui->form_enter->setGeometry( event->size().width() / 2 - 150, event->size().height() / 2 - 130, 300, 150 );
    ui->txtErrorMessage->setGeometry( 100, 125, 201, 20 );
    ui->config_tabs->setGeometry( 250, 20, event->size().width() - 271, event->size().height() - 82 );
    ui->tools_tabs->setGeometry( 20, 20, 221, event->size().height() - 82 );
}

void ViewTerminal::initUI()
{
    _tickTimer = 100;
    _timer->setInterval( _tickTimer );

    ui->txtErrorMessage->setVisible( false );


    ui->config_tabs->addTab( scene3D, "3D" );
    ui->config_tabs->addTab( codeEditor, "Gcode");

    loadConfig = new QPushButton( "Load config" );
    applyConfig = new QPushButton( "Apply config" );
    saveConfig = new QPushButton( "Save config" );

    info = new QPushButton( "Info" );

    loadConfig->setMinimumHeight( 29 );
    applyConfig->setMinimumHeight( 29 );
    saveConfig->setMinimumHeight( 29 );
    info->setMinimumHeight( 29 );

    connect( loadConfig, SIGNAL(clicked()), this, SLOT(loadConfiguratePrinter()) );
    connect( applyConfig, SIGNAL(clicked()), this, SLOT(applyConfiguratePrinter()) );
    connect( saveConfig, SIGNAL(clicked()), this, SLOT(saveConfiguratePrinter()) );
    connect( info, SIGNAL(clicked()), this, SLOT(infoAbout()) );

    ui->header_layout->insertWidget( 0, info );


    connect( ui->lineAcceleration, SIGNAL(textChanged(QString)), this, SLOT(changeConfigurationPrinter()) );
    connect( ui->lineLayerThickness, SIGNAL(textChanged(QString)), this, SLOT(changeConfigurationPrinter()) );
    connect( ui->lineLowerLimitPositioning, SIGNAL(textChanged(QString)), this, SLOT(changeConfigurationPrinter()) );
    connect( ui->lineMaxHeightPositioning, SIGNAL(textChanged(QString)), this, SLOT(changeConfigurationPrinter()) );
    connect( ui->lineMaxSpeed, SIGNAL(textChanged(QString)), this, SLOT(changeConfigurationPrinter()) );
    connect( ui->lineMaxSpeedPrinthead, SIGNAL(textChanged(QString)), this, SLOT(changeConfigurationPrinter()) );
    connect( ui->lineMaxSpeedScanner, SIGNAL(textChanged(QString)), this, SLOT(changeConfigurationPrinter()) );
    connect( ui->lineMinHeightPositioning, SIGNAL(textChanged(QString)), this, SLOT(changeConfigurationPrinter()) );
    connect( ui->lineSpeedG00Axis, SIGNAL(textChanged(QString)), this, SLOT(changeConfigurationPrinter()) );
    connect( ui->lineUpperLimitPositioning, SIGNAL(textChanged(QString)), this, SLOT(changeConfigurationPrinter()) );
    connect( ui->lineWidthScanner, SIGNAL(textChanged(QString)), this, SLOT(changeConfigurationPrinter()) );
    connect( ui->lineWidthNozzle, SIGNAL(textChanged(QString)), this, SLOT(changeConfigurationPrinter()) );

    connect( codeEditor, SIGNAL(textChanged()), this, SLOT(changedCommand()) );
    connect( workstation, SIGNAL(changedPosition(double,double,double)), scene3D, SLOT(changedPosition(double,double,double)) );
    connect( workstation, SIGNAL(changedPosition(double,double,double)), ui->scene2D, SLOT(changedPosition(double,double)) );
    connect( workstation, SIGNAL(changedPrint(bool,int)), ui->scene2D, SLOT(newLayer(bool)) );
    connect( workstation, SIGNAL(changedPrint(bool,int)), scene3D, SLOT(changedPrint(bool,int)) );

    connect( _timer, SIGNAL(timeout()), this, SLOT(stepTick()) );

    connect( ui->stepPause, SIGNAL(clicked()), _timer, SLOT(stop()) );
    connect( ui->stepResume, SIGNAL(clicked()), _timer, SLOT(start()) );
    connect( controller, SIGNAL(executedCommand(Command*)), this, SLOT(executedCommand(Command*)) );

    ui->checkprogram->setEnabled( false );
    ui->buildModel->setEnabled( false );
}

void ViewTerminal::changedCommand()
{
    ui->checkprogram->setEnabled(true);
    ui->buildModel->setEnabled(false);
}

void ViewTerminal::printInfoOfPrinter()
{
    ui->txtAxisXMaxSpeed->setText(
                QString::number( workstation->getAxisX()->getMaxSpeed() ) + " mm/s" );
    ui->txtAxisXAcceleration->setText(
                QString::number( workstation->getAxisX()->getAcceleration() ) + " mm/s2" );
    ui->txtAxisXSpeedG0->setText(
                QString::number( workstation->getAxisX()->getSpeedG00() ) + " mm/s");

    ui->txtAxisYMaxSpeed->setText(
                QString::number( workstation->getAxisY()->getMaxSpeed() ) + " mm/s" );
    ui->txtAxisYAcceleration->setText(
                QString::number( workstation->getAxisY()->getAcceleration() ) + " mm/s2" );
    ui->txtAxisYSpeedG0->setText(
                QString::number( workstation->getAxisY()->getSpeedG00() ) + " mm/s");

    ui->txtAxisZMaxSpeed->setText(
                QString::number( workstation->getAxisZ()->getMaxSpeed() ) + " mm/s" );
    ui->txtAxisZAcceleration->setText(
                QString::number( workstation->getAxisZ()->getAcceleration() ) + " mm/s2" );
    ui->txtAxisZSpeedG0->setText(
                QString::number( workstation->getAxisZ()->getSpeedG00() ) + " mm/s");

    ui->txtPrintheadLayerThickness->setText(
                QString::number( workstation->getPrinthead()->getLayerThickness() ) );
    ui->txtPrintheadMaxHeight->setText(
                QString::number( workstation->getPrinthead()->getMaxHeight() ) );
    ui->txtPrintheadMinHeight->setText(
                QString::number( workstation->getPrinthead()->getMinHeight() ) );
    ui->txtPrintheadWidthNozzle->setText(
                QString::number( workstation->getPrinthead()->getWidthNozzle() ) );

    ui->txtScannerMaxSpeed->setText(
                QString::number( workstation->getScanner()->getMaxSpeed() ) );
    ui->txtScannerWidthLine->setText(
                QString::number( workstation->getScanner()->getWidthLine() ) );
    ui->txtMaxHeightScanner->setText(
                QString::number( workstation->getScanner()->getMaxHeight() ) );
    ui->txtMinHeightScanner->setText(
                QString::number( workstation->getScanner()->getMinHeight() ) );
}

void ViewTerminal::loadConfiguratePrinter()
{
    ui->lineAcceleration->setText( QString::number( configWorkstation->GetAcceleration() ) );
    ui->lineMaxSpeed->setText( QString::number( configWorkstation->GetMaxSpeedAxis() ) );
    ui->lineSpeedG00Axis->setText( QString::number( configWorkstation->GetSpeedG00Axis() ) );
    ui->lineMinHeightPositioning->setText( QString::number( configWorkstation->GetMinHeightPositioning() ) );
    ui->lineMaxHeightPositioning->setText( QString::number( configWorkstation->GetMaxHeightPositioning() ) );
    ui->lineUpperLimitPositioning->setText( QString::number( configWorkstation->GetUpperLimitPositioning() ) );
    ui->lineLowerLimitPositioning->setText( QString::number( configWorkstation->GetLowerLimitPositioning() ) );


    ui->lineWidthNozzle->setText( QString::number( configWorkstation->GetWidthNozzle() ) );
    ui->lineLayerThickness->setText( QString::number( configWorkstation->GetLayerThicness() ) );
    ui->lineMaxSpeedPrinthead->setText( QString::number( configWorkstation->GetMaxSpeedPrinthead() ) );


    ui->lineMaxSpeedScanner->setText( QString::number( configWorkstation->GetMaxSpeedScanner() ) );
    ui->lineWidthScanner->setText( QString::number( configWorkstation->GetWidthScanner() ) );
    ui->tabWidget->setTabText( 0, "Настройка параметров станка");
}

void ViewTerminal::applyConfiguratePrinter()
{
    configWorkstation->SetAcceleration( ui->lineAcceleration->text().toInt() );
    configWorkstation->SetLayerThicness( ui->lineLayerThickness->text().toInt() );
    configWorkstation->SetLowerLimitPositioning( ui->lineLowerLimitPositioning->text().toInt() );
    configWorkstation->SetMaxHeightPositioning( ui->lineMaxHeightPositioning->text().toInt() );
    configWorkstation->SetMaxSpeedAxis( ui->lineMaxSpeed->text().toInt() );
    configWorkstation->SetMaxSpeedPrinthead( ui->lineMaxSpeedPrinthead->text().toInt() );
    configWorkstation->SetMaxSpeedScanner( ui->lineMaxSpeedScanner->text().toInt() );
    configWorkstation->SetMinHeightPositioning( ui->lineMinHeightPositioning->text().toInt() );
    configWorkstation->SetWidthScanner( ui->lineWidthScanner->text().toInt() );
    configWorkstation->SetWidthNozzle( ui->lineWidthNozzle->text().toInt() );
    configWorkstation->SetUpperLimitPositioning( ui->lineUpperLimitPositioning->text().toInt() );
    configWorkstation->SetSpeedG00Axis( ui->lineSpeedG00Axis->text().toInt() );
    (new XMLConfigurationDAO())->UpdateConfiguration( configWorkstation );

     ui->tabWidget->setTabText( 0, "Настройка параметров станка");
}

void ViewTerminal::infoAbout()
{
    ui->pages->setCurrentIndex( 3 );
}

void ViewTerminal::saveConfiguratePrinter()
{
    QString fileName = QFileDialog::getSaveFileName(this,
                                     tr("Save configurate file"), "",
                                     tr("XML file (*.xml);;All Files (*)"));

    configWorkstation->save( fileName );
}

void ViewTerminal::changeConfigurationPrinter()
{
    ui->tabWidget->setTabText( 0, "Настройка параметров станка *");
}

void ViewTerminal::stepTick()
{
    controller->executeCommand();
    ui->header_layout->removeWidget( info );
}

ViewTerminal::~ViewTerminal()
{
    delete ui;
}

void ViewTerminal::on_menu_exit_clicked()
{
    if( ui->pages->currentIndex() == 0 )
    {
        this->close();
        return ;
    }
    ui->pages->setCurrentIndex( 0 );
    ui->txtTypeAccount->setVisible( false );
    ui->labelEnter->setVisible( false );


    ui->header_layout->removeWidget( loadConfig );
    ui->header_layout->removeWidget( applyConfig );
    ui->header_layout->removeWidget( saveConfig );
    ui->header_layout->insertWidget( 0, info );
}

void ViewTerminal::on_enter_config_clicked()
{

    if( configWorkstation->GetAdminPwd().toUtf8() == QCryptographicHash::hash( ui->line_password->text().toUtf8(), QCryptographicHash::Sha3_512 ).toHex() )
    {

        ui->header_layout->removeWidget( info );
        ui->txtErrorMessage->setVisible( false );
        ui->txtTypeAccount->setVisible( true );
        ui->labelEnter->setVisible( true );
        ui->txtTypeAccount->setText( ui->line_login->text() + "(Мастер настройки)");
        ui->pages->setCurrentIndex( 2 );

        ui->header_layout->insertWidget( 0, loadConfig );
        ui->header_layout->insertWidget( 1, applyConfig );
        ui->header_layout->insertWidget( 2, saveConfig );
    }
    else
    {
        ui->txtErrorMessage->setVisible( true );
        qDebug() << ui->txtErrorMessage->geometry();
        qDebug() << ui->form_enter->geometry();
    }
}

void ViewTerminal::on_enter_operator_clicked()
{
    ui->pages->setCurrentIndex( 1 );
    ui->txtTypeAccount->setVisible( true );
    ui->labelEnter->setVisible( true );
    ui->txtTypeAccount->setText( "Оператор станка" );
}

void ViewTerminal::on_loadProgramm_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                     tr("Open file gcode"), "",
                                     tr("GCode file (*.gcode);;All Files (*)"));

    QFile *file = new QFile( fileName );
    if (!file->open( QIODevice::ReadOnly ) )
    {
        ui->logLoadGCode->setPlainText( "File " + fileName + " not found!" );
        return ;
    }

    ui->config_tabs->setCurrentWidget( codeEditor );
    ui->logLoadGCode->setPlainText( "Load file: " + fileName + "\n" );
    codeEditor->clear();
    codeEditor->appendPlainText( file->readAll() );
    ui->checkprogram->setEnabled( true );
    file->close();
    delete file;
}

void ViewTerminal::on_checkprogram_clicked()
{
    try
    {
        controller->setCommand( codeEditor->toPlainText().split( '\n' ));
        controller->checkCommand();
        ui->txtCountLayer->setText( QString::number( controller->getCountLayer() ) );
        ui->txtCycleScanning->setText( QString::number( controller->getCountCycleScannig() ) );
        ui->txtCountMFunction->setText( QString::number( controller->getCountMfunction() ) );
        ui->txtTime->setText( QString::number( controller->getTime() / 10 ) + " min" );
        ui->selectLayer->setMaximum( controller->getCountLayer() );
        ui->buildModel->setEnabled( true );
        ui->logLoadGCode->appendPlainText( "Входной файл корректно разобран." );
    }
    catch ( IllegalStateException ex )
    {
        ui->logLoadGCode->appendPlainText( ex.toString() );
        ui->logLoadGCode->appendPlainText( "In line: " + QString::number( controller->getNumberCommand() ) );

    }
}

void ViewTerminal::on_addMFunction_clicked()
{
    ui->tableMFunction->insertRow( ui->tableMFunction->rowCount() );
    QComboBox *combo = new QComboBox(this);
    combo->addItem("After start");
    combo->addItem("Before print");
    combo->addItem("After print");
    combo->addItem("Before scan");
    combo->addItem("After scan");
    combo->addItem("Before end");
    ui->tableMFunction->setCellWidget( ui->tableMFunction->rowCount()-1, 2, combo);
}

void ViewTerminal::executedCommand(Command *command)
{
    ui->currentCommandExecute->appendPlainText( command->getLine() );
}

void ViewTerminal::on_buildModel_clicked()
{
    ui->config_tabs->setCurrentWidget( scene3D );
    scene3D->setWidthNozzle( configWorkstation->GetWidthNozzle() );
    controller->execute();
}

void ViewTerminal::on_stepResume_clicked()
{    
    scene3D->setWidthNozzle( configWorkstation->GetWidthNozzle() );
    ui->config_tabs->setCurrentWidget( scene3D );
    _timer->start( 100 );
}

void ViewTerminal::on_stepSlow_clicked()
{
    _tickTimer += 10;
    _timer->setInterval( _tickTimer );
}

void ViewTerminal::on_stepFast_clicked()
{
    _tickTimer -= 10;
    _timer->setInterval( _tickTimer );
}

void ViewTerminal::on_stepRestart_clicked()
{
    scene3D->clear();
    _timer->stop();
    controller->reset();
    ui->currentCommandExecute->clear();
}

void ViewTerminal::on_editPassword_clicked()
{
    if ( ui->lineNewPassword->text() == ui->lineNewPasswordR->text() )
    {
        configWorkstation->SetAdminPwd( ui->lineOldPassword->text(), ui->lineNewPassword->text() );
        (new XMLConfigurationDAO())->UpdateConfiguration( configWorkstation );
    }
}

void ViewTerminal::on_selectLayer_valueChanged(int value)
{
    ui->scene2D->setCurrentLayer( value );
}

void ViewTerminal::on_pushButton_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this,
                                     tr("Save file gcode"), "",
                                     tr("GCode file (*.gcode);;All Files (*)"));

    QFile *file = new QFile( fileName );
    if (!file->open( QIODevice::WriteOnly ) )
    {
        ui->logLoadGCode->setPlainText( "File " + fileName + " not found!" );
        return ;
    }

    file->write( codeEditor->toPlainText().toUtf8() );
    ui->logLoadGCode->appendPlainText( "File saved to: " + fileName );
    file->close();
    delete file;
}
