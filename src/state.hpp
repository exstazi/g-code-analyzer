#ifndef STATE_HPP
#define STATE_HPP

#include <QString>
#include <QMap>
#include <QDebug>
#include "command/command.h"

#include "blockalloc.h"

/**
 * Интерфейс для сравнения элементов в контейнере
 * @tempalte T - хранимый тип в контейнере
 */
template <typename T>
class IComparate
{
public:

    virtual bool compare( T first, T second ) = 0;
};

/**
 * Реализация для сравнения типов Command*
 */
class ICompareCommand: public IComparate<Command*>
{
public:
    bool compare(Command *first, Command *second)
    {
        return (first->getName() == second->getName());
    }
};

template <typename Transition>
struct NodeTransition;

/**
 * Структура для хранения состояния внутри контейнера
 * @param _name - имя состояния
 * @param _transitions - список возможнных переходов
 * @param _nextState - список всех состояний
 */
template <typename Transition>
struct NodeState: public BlockAlloc<NodeState<Transition> >
{
    QString _name;
    NodeTransition<Transition> *_transitions;
    NodeState<Transition> *_nextState;

    NodeState(QString name, NodeTransition<Transition> *transition = NULL,
              NodeState<Transition> *nextState = NULL )
    {
        _name = name;
        _transitions = transition;
        _nextState = nextState;
    }
};

/**
 * Структура для хранения правил перехода внутри контейнера
 * @param _isSuperState - данный переход
 * @param _rule - токен перехода
 * @param _state - состояние в которое можно перейти
 * @param _nextTransition - список возможных переходов
 */
template <typename Transition>
struct NodeTransition: public BlockAlloc<NodeTransition<Transition> >
{
    bool _isSuperState;
    Transition _rule;
    NodeState<Transition> *_state;
    NodeTransition<Transition> *_nextTransition;
};

#endif // STATE_HPP

