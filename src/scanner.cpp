#include "scanner.h"

Scanner::Scanner(IConfiguration *configuration)
{
    maxSpeed = configuration->GetMaxSpeedScanner();
    widthLine = configuration->GetWidthScanner();
    minHeight = configuration->GetMinHeightPositioning();
    maxHeight = configuration->GetMaxHeightPositioning();
}

long Scanner::getMaxSpeed()
{
    return maxSpeed;
}

int Scanner::getWidthLine()
{
    return widthLine;
}

int Scanner::getMinHeight()
{
    return minHeight;
}

int Scanner::getMaxHeight()
{
    return maxHeight;
}
