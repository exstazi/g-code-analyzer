#ifndef WORKSTATION_H
#define WORKSTATION_H

#include <QObject>
#include <QFile>

#include "printhead.h"
#include "scanner.h"
#include "axis.h"

class Workstation: public QObject
{
    Q_OBJECT
public:
    Workstation( IConfiguration* configuration );

    Axis* getAxisX() { return _x; }
    Axis* getAxisY() { return _y; }
    Axis* getAxisZ() { return _z; }
    Printhead* getPrinthead() { return printhead; }
    Scanner* getScanner() { return scanner; }

    void move( double x, double y, double z );
    void print( bool status );
signals:
    void changedPosition( double x, double y, double z );
    void changedPrint( bool status, int widthNozzle );
protected:
    Axis* _x;
    Axis* _y;
    Axis* _z;
    Printhead* printhead;
    Scanner* scanner;
};

#endif // WORKSTATION_H
