#ifndef AXIS_H
#define AXIS_H

#include "iconfiguration.h"

/**
 * Структура хранения оси
 * @param _maxSpeed - максимальная скорость передвижения оси
 * @param _speedG00 - скорость для функции G0
 * @param _acceleration - ускорение оси
 * @param _upperLimitPosition - максимальная позиция
 * @param _currentPosition - текушая позиция
 * @param _currentSpeed - текущая скорость
 */
class Axis
{
public:
    Axis( IConfiguration *configuration );
    long getMaxSpeed();
    long getSpeedG00();
    long getAcceleration();
    long getUpperLimitPositioning();
    double getCurrentPosition();
    double getCurrentSpeed();

    void move( double position );
    void move( double position, double currentSpeed );

protected:
    long _maxSpeed;
    long _speedG00;
    long _acceleration;
    long _upperLimitPositioning;

    double _currentPosition;
    double _currentSpeed;

};

#endif // AXIS_H
