#ifndef VIEWTERMINAL_H
#define VIEWTERMINAL_H

#include <QWidget>
#include <QResizeEvent>
#include <QList>
#include <QPushButton>
#include <QTimer>

#include "xmlconfigurationdao.h"
#include "command/command.h"
#include "scene3d.h"
#include "scene2d.h"
#include "workstation.h"
#include "codeeditor.h"

#include "controller.h"


namespace Ui {
class ViewTerminal;
}

class ViewTerminal : public QWidget
{
    Q_OBJECT

public:
    explicit ViewTerminal(QWidget *parent = 0);
    ~ViewTerminal();

protected:
    void resizeEvent(QResizeEvent *event);

    void printInfoOfPrinter();

    void initUI();

private slots:
    void on_menu_exit_clicked();

    void on_enter_config_clicked();

    void on_loadProgramm_clicked();

    void on_checkprogram_clicked();

    void on_enter_operator_clicked();


    void loadConfiguratePrinter();
    void applyConfiguratePrinter();
    void saveConfiguratePrinter();
    void infoAbout();

    void changeConfigurationPrinter();
    void changedCommand();
    void executedCommand(Command *command);

    void on_addMFunction_clicked();

    void on_buildModel_clicked();

    void on_stepResume_clicked();
    void stepTick();

    void on_stepSlow_clicked();

    void on_stepFast_clicked();

    void on_stepRestart_clicked();

    void on_editPassword_clicked();

    void on_selectLayer_valueChanged(int value);

    void on_pushButton_clicked();

private:
    Ui::ViewTerminal *ui;
    Workstation *workstation;
    CodeEditor *codeEditor;
    Scene3D *scene3D;
    XMLConfigurationFile *configWorkstation;
    Controller *controller;
    QTimer *_timer;
    int _tickTimer;

    QPushButton *loadConfig;
    QPushButton *applyConfig;
    QPushButton *saveConfig;

    QPushButton *info;
};

#endif // VIEWTERMINAL_H
