#ifndef XMLDAOFACTORY_H
#define XMLDAOFACTORY_H

#include "xmlconfigurationdao.h"

class XMLDAOFactory
{
public:
    static IConfigurationDAO* GetConfigurationDAO();
protected:
    XMLDAOFactory(){}
    static XMLConfigurationDAO *_config;
};

#endif // XMLDAOFACTORY_H
