#ifndef STATEMACHINE_HPP
#define STATEMACHINE_HPP

#include <QStack>

#include "state.hpp"
#include "exception.h"
#include "json.h"
#include "smiterator.hpp"

/**
 *  Шаблонный контейнер state машины с супер состояними
 *  @template Transition - тип токенов
 *  @param _head - указатель на начало контейнера
 *  @param _last - указатель на последний элемент контейнера
 *  @param _initState - начальное состояние
 *  @param _currentState - текущие состояние
 *  @param _endState - конечное состояние
 */
template <typename Transition>
class StateMachine
{
public:
    typedef SMIterator<Transition> iterator;
    typedef SMIterator<const Transition> const_iterator;

    StateMachine();
    StateMachine( const StateMachine &st );
    ~StateMachine();

    void insertState( QString nameState, QString nameNextState, Transition transition, bool isSuperState = false );
    void checkToken( Transition token, IComparate<Transition> *compare );
    void setInitState( QString nameState );
    void setEndState( QString nameState );
    QString getCurrentState();
    bool isEndState();
    void reset();
    bool empty();
    int size();
    void clear();
    void erase(QString nameState);

    void serialize(QString fileName);
    void deserialize(QString fileName);


    iterator begin() { return SMIterator<Transition>( _initState ); }
    iterator end() { return SMIterator<Transition>( _last ); }

    const_iterator begin() const;
    const_iterator end() const;

    void operator=(const StateMachine<Transition> &other);
    bool operator==(const StateMachine<Transition> &other);


protected:
    NodeState<Transition>* getState( QString name );

    NodeState<Transition> *_head;
    NodeState<Transition> *_last;

    NodeState<Transition> *_initState;
    NodeState<Transition> *_currentState;
    NodeState<Transition> *_endState;

    QStack<NodeState<Transition>* > _superStates;
};


template <typename Transition>
StateMachine<Transition>::StateMachine()
{
    _currentState = _head = new NodeState<Transition>( "" );
    _last = NULL;
}

template <typename Transition>
StateMachine<Transition>::StateMachine( const StateMachine &st )
{
    _head = st._head;
    _last = st._head;
    _initState = st._initState;
    _currentState = st._currentState;
    _endState = st._endState;

    NodeTransition<Transition> *p = NULL;
    for (SMIterator<Transition> iter = st.begin();
                    iter != st.end(); ++iter )
    {
        p = (*iter)->_transitions;
        while (p)
        {
            insert( (*iter)->_name, p->_state->_name, p->_rule, p->_isSuperState );
        }
    }
}

/**
 *  Cериализация контейнера в файл
 *  @param fileName - имя файла
 */
template <typename Token>
void StateMachine<Token>::serialize(QString fileName)
{
    QtJson::JsonObject contributor;
    contributor["name"] = "Luis Gustavo";
    contributor["age"] = 22;

    QtJson::Object json;

    NodeState<Token> *p = _head->_nextState;
    NodeTransition<Token> *tr = NULL;
    while (p)
    {
        tr = p->_transitions;
        while (tr)
        {
            json[p->_name][tr->_rule] = QString(tr->_state->_name);
            tr = tr->_nextTransition;
        }
        p = p->_nextState;
    }
    QFile *file = new QFile(fileName);
    if (!file->open(QIODevice::WriteOnly))
    {
        throw FileNotFoundException();
    }
    file->write( QtJson::serialize( json ) );
    file->close();
    delete file;
}

/**
 *  Десериализация контейнера в файл
 *  @param fileName - имя файла
 *  @exception FileNotFoundException - указанного файла не существует
 *  @exception JSONStateException - некооректный формат входного файла
 */
template <typename Token>
void StateMachine<Token>::deserialize(QString fileName)
{
    QFile *f = new QFile(fileName);
    if (!f->open(QFile::ReadOnly | QFile::Text)) {
        throw FileNotFoundException();
    }
    QTextStream in(f);
    QString json = in.readAll();
    if (json.isEmpty()) {
        throw JSONStateException();
    }
    bool ok;
    QtJson::JsonObject result = QtJson::parse(json, ok).toMap();
    if (!ok) {
         throw JSONStateException();
    }

    foreach (QString key, result.keys()) {
        foreach (QString key_2, result.value(key).toMap().keys()) {
            Command *command = Command::getInstance( key_2 );
            if (!command)
            {
                throw JSONStateException();
            }
            insertState( key, result.value(key).toMap().value( key_2 ).toString(), command );
        }
    }
}


/**
 *  Добавление состояния, если такое состояние существует, то к нему добавляются правила перехода
 *  @param name - имя добавляемого состояния
 *  @param transitions - переходы
 */
template <typename Token>
void StateMachine<Token>::insertState(QString nameState, QString nameNextState, Token transition, bool isSuperState)
{
    NodeTransition<Token> *currTransition = new NodeTransition<Token>();
    currTransition->_isSuperState = isSuperState;
    currTransition->_rule = transition;
    currTransition->_state = getState( nameNextState );
    if (!currTransition->_state)
    {
        currTransition->_state = new NodeState<Token>( nameNextState );
        if (!_last )
        {
            _last = currTransition->_state;
            _head->_nextState = _last;
        }
        else
        {
            _last->_nextState = currTransition->_state;
            _last = currTransition->_state;
        }
    }

    NodeState<Token> *current = getState( nameState );
    if (!current)
    {
        current = new NodeState<Token>( nameState );
        if (!_last)
        {
            _last = current;
            _head->_nextState = _last;
        }
        else
        {
            _last->_nextState = current;
            _last = current;
        }
    }

    if (!current->_transitions)
    {
        current->_transitions = currTransition;
        return ;
    }
    NodeTransition<Token> *tran = current->_transitions;
    while (tran->_nextTransition)
    {
        tran = tran->_nextTransition;
    }
    tran->_nextTransition = currTransition;
}

/**
 * Поиск состояния в списке по его имени
 * @return указатель на состояние, NULL если такого состояния нет
 */
template <typename Token>
NodeState<Token>* StateMachine<Token>::getState(QString name)
{
    NodeState<Token> *current = _head;
    while (current)
    {
        if (current->_name == name )
        {
            return current;
        }
        current = current->_nextState;
    }
    return NULL;
}

/**
 *  Проверка токена на выполнимость state мишиной
 *  @param token - входной токен
 *  @exception IllegalStateException - возникает если нет состояния в которое можно перейти по входному токену
 */
template <typename Token>
void StateMachine<Token>::checkToken(Token token, IComparate<Token> *compare)
{
    NodeTransition<Token> *transition = _currentState->_transitions;
    while (transition)
    {
        if (compare->compare(transition->_rule, token))
        {
            if (transition->_isSuperState)
            {
                _superStates.push(_currentState);
                //qDebug() << "Переход по суперсостоянию";
            }
            _currentState = transition->_state;
            //qDebug() << "Переход в состояние " << _currentState->_name;
            return ;
        }
        transition = transition->_nextTransition;
    }
    if (_superStates.size() > 0 )
    {
        _currentState = _superStates.pop();
        checkToken(token, compare);
        //qDebug() << "Pop stack";
        return ;
    }
    throw IllegalStateException();
}

/**
 *  Установка начального состояния
 *  @param nameState - имя состояния
 */
template <typename Token>
void StateMachine<Token>::setInitState(QString nameState)
{
    _currentState = getState( nameState );
    _initState = _currentState;
    if (!_currentState)
    {
        throw IllegalStateException( "State '" + nameState + "' not found!");
    }
    /*_initStateState = _states->value( nameState, NULL );
    if (!_currentStateState)
    {
        _currentStateState = _initStateState;
    }
    qDebug() << "Set init state " << nameState << " " << *_currentStateState->getTransitions();*/
}

/**
 *  Установка конечного состояния
 *  @param nameState - имя состояния
 */
template <typename Token>
void StateMachine<Token>::setEndState(QString nameState)
{
    _endState = getState( nameState );
    if (!_endState)
    {
        throw IllegalStateException( "State '" + nameState + "' not found!");
    }
    /*_endStateState = _states->value( nameState, NULL );
    qDebug() << "Set end state " << nameState;*/
}

/**
 *  @return имя состояния
 */
template <typename Token>
QString StateMachine<Token>::getCurrentState()
{
    return _currentState->_name;
}

/**
 *  Находится ли state машина в конченом состоянии
 *  @return true - да конечное, false - иначе
 */
template <typename Token>
bool StateMachine<Token>::isEndState()
{
    return (_currentState == _endState);
}

/**
 *  Сбросить состояние state машины к начальному
 */
template <typename Token>
void StateMachine<Token>::reset()
{
    _currentState = _initState;
}

/**
 *  Очистить контейнер
 */
template <typename Token>
void StateMachine<Token>::clear()
{
    NodeState<Token> *p = _head;
    NodeState<Token> *prev = p;
    while (p)
    {
        prev = p->_nextState;
        delete p;
        p = prev;
    }
}

/**
 *  Проверка на пустой контейнер
 */
template <typename Token>
bool StateMachine<Token>::empty()
{
    return (begin() == end());
}

/**
 *  Удалить состояние из контейнера
 */
template <typename Token>
void StateMachine<Token>::erase(QString nameState)
{
    NodeState<Token> *p = _head;
    NodeState<Token> *prev = p;
    while (p->_name != nameState )
    {
        prev = p;
        p = p->_nextState;
    }
    prev->_nextState = p->_nextState;
    delete p;
}


/**
 *
 */
template <typename Transition>
StateMachine<Transition>::~StateMachine()
{
    clear();
}

#endif // STATEMACHINE_HPP

