#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>
class Command;
#include "command/concretecommand.h"
#include "workstation.h"
#include "statemachine.hpp"
#include <QMetaType>

class Controller: public QObject
{
    Q_OBJECT
public:
    Controller( Workstation *workstation );
    void processFile( QString nameFile );
    void setCommand( QList<QString> commands );
    void checkCommand();
    void reset();
    void execute();
    void executeCommand();

    int getTime();
    int getCountMfunction();
    int getCountCycleScannig();
    int getCountLayer();
    int getNumberCommand();

    QList<Command*>* getCommands();


protected:
    Command* parseCommand( QMap<char, double> *parameters );
    QMap<char, double> *getParametrsCommand( QByteArray lineCommand );

signals:
    void executedCommand( Command *command );

protected:
    int _time;
    int _countMfunction;
    int _countCycleScannig;
    int _countLayer;
    int _currentCommand;
    int _currentCheck;
    Workstation *_workstation;
    QList<Command*> *_commands;
    StateMachine<Command*> *_stateMachine;
};

#endif // CONTROLLER_H
