#include "scene3d.h"

#include <GL/glu.h>


#include <QDebug>

Scene3D::Scene3D(QWidget *parent) : QGLWidget(parent)
{
    scale = 0.3f;
    rotate_x = 0; rotate_y = rotate_z = 0;
    offset_x = offset_y = offset_z = 0;
    setAttribute(Qt::WA_OpaquePaintEvent);
    position.x = 0; position.y = 0; position.z = 0;
    view.x = 0; view.y = -100; view.z = 0;

    _list = new QList<CVector3<float> >();
    _limitPositioning.x = _limitPositioning.y = _limitPositioning.z = 10;
}

void Scene3D::setLimitPositioning(float x, float y, float z)
{
    _limitPositioning.x = x;
    _limitPositioning.y = y;
    _limitPositioning.z = z;
}

void Scene3D::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();


    glScalef(scale, scale, scale);
    glTranslatef( offset_x, offset_y, offset_z );
    glRotatef(rotate_x, 1.0f, 0.0f, 0.0f);
    glRotatef(rotate_y, 0.0f, 1.0f, 0.0f);
    glRotatef(rotate_z, 0.0f, 0.0f, 1.0f);

    /*gluLookAt( position.x, position.y, position.z,
               view.x, view.y, view.z,
               0,1,1 );
    */

    printGrid( 30.0, 30.0 );
    glTranslatef( offset_x - 15, offset_y - 15, offset_z );


    glLineWidth( _widthNozzle );
    glColor3f( 1, 0, 0 );
    glBegin( GL_LINE_STRIP );
    foreach (CVector3<float> point, *_list) {
        glVertex3f( point.x, point.y, point.z );
    }
    glEnd();


/*
    glColor3f (1.0, 1.0, 1.0);
    glEnableClientState(GL_VERTEX_ARRAY);
        glVertexPointer(3,GL_FLOAT,sizeof(CVector3<float>),_list->toVector(). );
        glDrawArrays(GL_LINES,0,_list->size());
    glDisableClientState(GL_VERTEX_ARRAY);
*/
}

void Scene3D::changedPrint(bool status, int widhtNozzle)
{
    _isPrinting = status;
    _widthNozzle = widhtNozzle;
}

void Scene3D::setWidthNozzle(int wdthNozzle)
{
    _widthNozzle = wdthNozzle;
}

void Scene3D::changedPosition(double x, double y, double z)
{
    if (!_isPrinting )
        return ;
    if (!_list)
    {
        _list = new QList<CVector3<float> >();
    }
    _list->append( CVector3<float>( x / 10, y / 10, z / 10) );
    updateGL();
}

void Scene3D::clear()
{
    _list->clear();
    updateGL();
}

void Scene3D::printGrid(GLfloat w, GLfloat h)
{
    glLineWidth( 1.0 );
    glColor3f( 0.5, 0.5, 0.5 );
    glBegin( GL_LINES );
    for( GLfloat i = -w/2; i <= w/2; i += 0.5 )
    {
        glVertex3f( i,  -h/2,  0 );
        glVertex3f( i,  h/2,  0 );
    }
    for( GLfloat i = -h/2; i <= h/2; i += 0.5 )
    {
        glVertex3f( -w/2,  i,  0 );
        glVertex3f( w/2,  i,  0 );
    }
    glEnd();

    glColor3f( 0.8, 0.8, 0.8 );
    glBegin( GL_LINES );
    for( GLfloat i = -w/2; i <= w/2; i += 0.1 )
    {
        glVertex3f( i,  -h/2,  0 );
        glVertex3f( i,  h/2,  0 );
    }
    for( GLfloat i = -h/2; i <= h/2; i += 0.1 )
    {
        glVertex3f( -w/2,  i,  0 );
        glVertex3f( w/2,  i,  0 );
    }
    glEnd();
}

void Scene3D::initializeGL() {
    qglClearColor( QColor( 249,249, 198 ) );
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    // Сглаживание точек
    glEnable(GL_POINT_SMOOTH);
    glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
    // Сглаживание линий
    glEnable(GL_LINE_SMOOTH);
    glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
    // Сглаживание полигонов
    glEnable(GL_POLYGON_SMOOTH);
    glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);


}

void Scene3D::resizeGL(int nWidth, int nHeight )
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    GLfloat ratio=(GLfloat)nHeight/(GLfloat)nWidth;

    if (nWidth>=nHeight) {
        glOrtho(-1.0/ratio, 1.0/ratio, -1.0, 1.0, -10.0, 1.0);
    } else {
        glOrtho(-1.0, 1.0, -1.0*ratio, 1.0*ratio, -10.0, 1.0);
    }

    glViewport(0, 0, (GLint)nWidth, (GLint)nHeight);
}

void Scene3D::wheelEvent(QWheelEvent *event)
{
    if( event->delta() > 0 ) {
        scale *= 1.1;
    } else {
        scale /= 1.1;
    }
    updateGL();
}

void Scene3D::mousePressEvent(QMouseEvent* event)
{
    ptrMousePosition = event->pos();
}

void Scene3D::mouseMoveEvent(QMouseEvent* event)
{
    if( event->buttons() & Qt::LeftButton )
    {
        rotate_x += 180/* /scale */ * (GLfloat)(event->y()-ptrMousePosition.y())/height();
        rotate_z += 180/* /scale */ * (GLfloat)(event->x()-ptrMousePosition.x())/width();
        ptrMousePosition = event->pos();
        updateGL();
    }
    else
    {
        offset_x += (event->x() - ptrMousePosition.x()) / 100;
        updateGL();
    }
}
