#ifndef BLOCKALLOC_H
#define BLOCKALLOC_H

#include <QVector>
#include <QDebug>
#include <windows.h>

inline size_t align(size_t x, size_t a) { return ((x-1) | (a-1)) + 1; }

/**
 *  Выделение пула памяти
 *  @tempalate T - тип хранимых данных
 */
template<typename T, size_t PageSize = 65536, size_t Alignment = 8>
class BlockPool
{
public:
    BlockPool() : head(NULL) {
        BlockSize = align(sizeof(T), Alignment);
        count = PageSize / BlockSize;
    }

    /**
     * Создание блока памяти
     */
    void* AllocBlock()
    {
        if (!head) FormatNewPage();
        void* tmp = head;
        head = *(void**)head;
        return tmp;
    }

    /**
     * Добавление свободного блока
     */
    void FreeBlock(void* tmp)
    {
        *(void**)tmp = head;
        head = tmp;
    }

    /**
     * Выделение блока памяти
     */
    void* GetPage() {
        void* page = VirtualAlloc(NULL, PageSize, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
        pages.push_back(page);
        return page;
    }


    ~BlockPool() {
        for (QVector<void*>::iterator i = pages.begin(); i != pages.end(); ++i) {
            VirtualFree(*i, 0, MEM_RELEASE);
        }
    }
protected:
    /**
     * Разбиение блока памяти на размер хранимой структуры
     */
    void FormatNewPage() {
        void* tmp = GetPage();
        head = tmp;
        for(size_t i = 0; i < count-1; i++) {
                void* next = (char*)tmp + BlockSize;
                *(void**)tmp = next;
                tmp = next;
        }
        *(void**)tmp = NULL;
    }

    void* head;
    size_t BlockSize;
    size_t count;
    QVector<void*> pages;
};

/**
 * Аллокатор
 * @tempalte T - тип хранимых данных
 */
template<typename T>
class BlockAlloc
{
public:
    static void* operator new(size_t s) {
        if (s != sizeof(T)) {
                return ::operator new(s);
        }
        return pool.AllocBlock();
    }
    static void operator delete(void* m, size_t s) {
        if (s != sizeof(T)) {
                ::operator delete(m);
        } else if (m != NULL) {
                pool.FreeBlock(m);
        }
    }

private:
        static BlockPool<T> pool;
};
template<class T>
BlockPool<T> BlockAlloc<T>::pool;

#endif // BLOCKALLOC_H
