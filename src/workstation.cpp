#include "workstation.h"

#include <QDebug>

Workstation::Workstation(IConfiguration *configuration)
{
    _x = new Axis( configuration );
    _y = new Axis( configuration );
    _z = new Axis( configuration );

    printhead = new Printhead( configuration );
    scanner = new Scanner( configuration );
}

void Workstation::move(double x, double y, double z)
{
    _x->move( x );
    _y->move( y );
    _z->move( z );
    emit changedPosition( _x->getCurrentPosition(), _y->getCurrentPosition(), _z->getCurrentPosition() );
}

void Workstation::print(bool status)
{
    emit changedPrint( status, printhead->getWidthNozzle() );
}
