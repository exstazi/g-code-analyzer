#ifndef IDAOFACTOTY
#define IDAOFACTOTY

#include "iconfigutationdao.h"

enum TypeDAO
{
    XML
};

class IDAOFactoty
{
public:
    virtual IConfigurationDAO* GetConfigurationDAO() = 0;
    virtual IDAOFactoty* GetDAOFactory(TypeDAO type) = 0;
};

#endif // IDAOFACTOTY

