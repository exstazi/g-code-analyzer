#ifndef CONCRETECOMMAND
#define CONCRETECOMMAND

#include <QVector3D>
#include "command/command.h"


class G00: public Command
{
public:
    G00( QVector3D vec = QVector3D(0,0,0) )
    {
        _position = vec;
    }

    void execute()
    {
        workstation->move( _position.x(), _position.y(), _position.z() );
    }

    QString getName()
    {
        return "G00";
    }

protected:
    QVector3D _position;
};


class G01: public Command
{
public:

    G01( QVector3D vec = QVector3D(0,0,0) )
    {
        _position = vec;
    }

    void execute()
    {
        workstation->move( _position.x(), _position.y(), _position.z() );
    }

    QString getName()
    {
        return "G01";
    }
protected:
    QVector3D _position;
};

class G02: public Command
{
public:

    G02( QVector3D vec = QVector3D(0,0,0) )
    {
        _position = vec;
    }

    void execute()
    {
        workstation->move( _position.x(), _position.y(), _position.z() );
    }

    QString getName()
    {
        return "G02";
    }
protected:
    QVector3D _position;
};

class G03: public Command
{
public:

    G03( QVector3D vec = QVector3D(0,0,0) )
    {
        _position = vec;
    }

    void execute()
    {
        workstation->move( _position.x(), _position.y(), _position.z() );
    }

    QString getName()
    {
        return "G03";
    }
protected:
    QVector3D _position;
};

class G04: public Command
{
public:

    G04( QVector3D vec = QVector3D(0,0,0) )
    {
        _position = vec;
    }

    void execute()
    {
        workstation->move( _position.x(), _position.y(), _position.z() );
    }

    QString getName()
    {
        return "G04";
    }
protected:
    QVector3D _position;
};


class M20: public  Command
{
public:

    void execute()
    {
        workstation->print( true );
    }

    QString getName()
    {
        return "M20";
    }
};

class M21: public  Command
{
public:

    void execute()
    {
        workstation->print( false );
    }

    QString getName()
    {
        return "M21";
    }
};

class M22: public  Command
{
public:

    void execute()
    {

    }

    QString getName()
    {
        return "M22";
    }
};

class M23: public  Command
{
public:

    void execute()
    {

    }

    QString getName()
    {
        return "M23";
    }
};

class M30: public  Command
{
public:

    void execute()
    {

    }

    QString getName()
    {
        return "M30";
    }
};

#endif // CONCRETECOMMAND

