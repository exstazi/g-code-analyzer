#include "command.h"
#include "concretecommand.h"

Command* Command::getInstance(QString name, QVector3D vec)
{
    if (name == "G0" || name == "G00")
        return new G00( vec );
    else if (name == "G1" || name == "G01")
        return new G01( vec );
    else if (name == "G2" || name == "G02")
        return new G02( vec );
    else if (name == "G3" || name == "G03")
        return new G03( vec );
    else if (name == "G4" || name == "G04")
        return new G04( vec );
    else if (name == "M20")
        return new M20();
    else if (name == "M21")
        return new M21();
    else if (name == "M22")
        return new M22();
    else if (name == "M23")
        return new M23();
    else if (name == "M30")
        return new M30();
    return NULL;
}


