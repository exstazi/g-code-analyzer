#ifndef COMMAND_H
#define COMMAND_H

#include <QObject>
#include <QVector3D>
#include <QObject>
#include "workstation.h"

class Workstation;

class Command
{
protected:
    Workstation *workstation;
    QString _line;
public:
    virtual ~Command(){}

    virtual void execute() = 0;
    void setWorkstation( Workstation *workstation )
    {
        this->workstation = workstation;
    }

    void setLine( QString line )
    {
        _line = line;
    }

    QString getLine()
    {
        return _line;
    }

    static Command *getInstance(QString name, QVector3D vec = QVector3D(0,0,0));

    virtual QString getName() = 0;



};


#endif // COMMAND_H
