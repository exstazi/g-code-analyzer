#-------------------------------------------------
#
# Project created by QtCreator 2016-04-05T00:03:12
#
#-------------------------------------------------

QT       += core gui
QT       += opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = gcode-analyzer
TEMPLATE = app


SOURCES += main.cpp\
    codeeditor.cpp \
    highlighter.cpp \
    viewterminal.cpp \
    xmlconfigurationfile.cpp \
    xmlconfigurationdao.cpp \
    xmldaofactory.cpp \
    scene3d.cpp \
    workstation.cpp \
    axis.cpp \
    printhead.cpp \
    scanner.cpp \
    controller.cpp \
    json.cpp \
    command/command.cpp \
    blockalloc.cpp \
    scene2d.cpp

HEADERS  += \
    codeeditor.h \
    highlighter.h \
    viewterminal.h \
    iconfiguration.h \
    xmlconfigurationfile.h \
    iconfigutationdao.h \
    xmlconfigurationdao.h \
    idaofactoty.h \
    xmldaofactory.h \
    scene3d.h \
    workstation.h \
    axis.h \
    printhead.h \
    scanner.h \
    state.hpp \
    statemachine.hpp \
    controller.h \
    smiterator.hpp \
    exception.h \
    json.h \
    command/concretecommand.h \
    command/command.h \
    blockalloc.h \
    scene2d.h

FORMS    += \
    viewterminal.ui

RESOURCES += \
    resource.qrc
