#include "scene2d.h"
#include <QDebug>

Scene2D::Scene2D(QWidget *parent) : QWidget(parent)
{
    _layers = new QList< QList<QVector2D> *>();
    _layers->append( new QList<QVector2D>() );
    _currentLayer = _countLayer = 0;
}

void Scene2D::paintEvent(QPaintEvent *)
{
    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing, true);
    p.setPen( Qt::NoPen );
    p.setBrush((QBrush(QColor( 249,249, 198 ))));
    p.drawRect(0,0,width(),height());
    p.setPen( Qt::black );

    p.drawText( 20,20, "Layer " + QString::number(_currentLayer+1) );
    QList<QVector2D>* currLayer = _layers->at( _currentLayer );

    for (int i = 1; i < currLayer->size(); ++i )
    {
        p.drawLine( currLayer->at(i-1).x(), currLayer->at(i-1).y(),
                    currLayer->at(i).x(), currLayer->at(i).y());
    }

}

void Scene2D::setCurrentLayer(int layer)
{
    if (layer < _layers->size() )
    {
        _currentLayer = layer;
        update();
    }
}

void Scene2D::changedPosition(double x, double y)
{
    if (_isPrint)
    {
        _layers->last()->append( QVector2D(40+x,5+y) );
        update();
    }
}

void Scene2D::newLayer(bool isPrint)
{
    _isPrint = isPrint;
    if (!isPrint)
    {
        _layers->append( new QList<QVector2D>() );
    }
}
