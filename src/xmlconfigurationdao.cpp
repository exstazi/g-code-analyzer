#include "xmlconfigurationdao.h"
#include <QDebug>

IConfiguration* XMLConfigurationDAO::configurationXmlFile = NULL;

XMLConfigurationDAO::XMLConfigurationDAO()
{
    configurationXmlFile = NULL;
}

IConfiguration *XMLConfigurationDAO::GetConfiguration()
{    
    if( configurationXmlFile == NULL )
    {
        return new XMLConfigurationFile( new QFile("configurate.xml") );
    }
    return configurationXmlFile;
}

void XMLConfigurationDAO::UpdateConfiguration(IConfiguration *configuration)
{
    if( configuration )
    {
        configurationXmlFile = configuration;
        configurationXmlFile->save("configurate.xml");
    }
}
