#ifndef PRINTHEAD_H
#define PRINTHEAD_H

#include "iconfiguration.h"

class Printhead
{
protected:
    int widthNozzle;
    int layerThickness;
    int minHeight;
    int maxHeight;

public:
    Printhead( IConfiguration *configuration );

    int getWidthNozzle();
    int getLayerThickness();
    int getMinHeight();
    int getMaxHeight();

};

#endif // PRINTHEAD_H
