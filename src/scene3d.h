#ifndef SCENE3D_H
#define SCENE3D_H

#include <QGLWidget>
#include <QWidget>
#include <QWheelEvent>
#include <QMouseEvent>

#include <QVector3D>

#include "command/command.h"
#include "workstation.h"


template <typename T>
struct CVector3 {
    T x,y,z;
    CVector3( T x1 = 0,T y1 = 0,T z1 = 0 ) {
        x = x1;
        y = y1;
        z = z1;
    }
};

class Scene3D : public QGLWidget
{
    Q_OBJECT
public:
    explicit Scene3D(QWidget *parent = 0);
    void setLimitPositioning( float x, float y, float z );
    void setWidthNozzle( int wdthNozzle );
    void clear();


protected:
    QList<CVector3<float> > *_list;
    CVector3<GLfloat> _limitPositioning;
    bool _isPrinting;
    GLfloat _widthNozzle;

    CVector3<GLfloat> position;
    CVector3<GLfloat> view;


    GLfloat rotate_x;
    GLfloat rotate_y;
    GLfloat rotate_z;
    GLfloat offset_x;
    GLfloat offset_y;
    GLfloat offset_z;
    GLfloat scale;

    QPoint ptrMousePosition;

    QTimer *timer;

    void initializeGL();
    void resizeGL(int nWidth, int nHeight );
    void wheelEvent(QWheelEvent* event);
    void mouseMoveEvent(QMouseEvent* event);
    void mousePressEvent(QMouseEvent* event);
    void paintGL();
    void printGrid(GLfloat w, GLfloat h );

protected slots:
    void changedPosition( double x, double y, double z );
    void changedPrint( bool status, int widhtNozzle );

};

#endif // SCENE3D_H
