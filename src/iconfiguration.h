#ifndef ICONFIGURATION
#define ICONFIGURATION

#include <QString>

class IConfiguration
{
public:
    virtual QString GetAdminPwd() = 0;
    virtual int GetWidthNozzle() = 0;
    virtual int GetLayerThicness() = 0;
    virtual int GetMinHeightPositioning() = 0;
    virtual int GetMaxHeightPositioning() = 0;
    virtual int GetMaxSpeedScanner() = 0;
    virtual int GetMaxSpeedPrinthead() = 0;
    virtual int GetWidthScanner() = 0;
    virtual int GetMaxSpeedAxis() = 0;
    virtual int GetSpeedG00Axis() = 0;
    virtual int GetAcceleration() = 0;
    virtual int GetUpperLimitPositioning() = 0;
    virtual int GetLowerLimitPositioning() = 0;

    virtual void save( QString nameFile ) = 0;
};

#endif // ICONFIGURATION

